package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Lens struct {
	label string
	value int
	next  *Lens
}

type Box struct {
	first *Lens
	last  *Lens
}

func (b *Box) remove(label string) {
	var prev *Lens
	for lens := b.first; lens != nil; lens = lens.next {
		if lens.label == label {
			if lens == b.first {
				b.first = lens.next
			}
			if lens == b.last {
				b.last = prev
			}
			if prev != nil {
				prev.next = lens.next
			}
			break
		}
		prev = lens
	}
}

func (b *Box) set(label string, value int) {
	for lens := b.first; lens != nil; lens = lens.next {
		if lens.label == label {
			lens.value = value
			return
		}
	}
	lens := &Lens{label, value, nil}
	if b.first == nil {
		b.first = lens
		b.last = lens
	} else {
		b.last.next = lens
		b.last = lens
	}
}

func (b Box) total() int {
	total := 0
	for lens, i := b.first, 1; lens != nil; lens, i = lens.next, i+1 {
		total += (i * lens.value)
	}
	return total
}

func (b Box) String() string {
	var sb strings.Builder
	for lens, i := b.first, 1; lens != nil; lens, i = lens.next, i+1 {
		sb.WriteString(fmt.Sprintf("[%s %d] ", lens.label, lens.value))
	}
	return sb.String()
}

func hash(s string) int {
	total := 0
	for _, r := range s {
		total += int(r)
		total *= 17
		total %= 256
	}
	return total
}

func printBoxes(boxes []Box) {
	for i := 0; i < len(boxes); i += 1 {
		if boxes[i].first == nil {
			continue
		}
		fmt.Printf("Box %d: %s\n", i, boxes[i].String())
	}
}

func processInput(f string) (int, error) {
	inputFile, err := os.Open(f)
	if err != nil {
		return -1, fmt.Errorf("Problem reading file at %s %w", f, err)
	}
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)
	cmdPattern := regexp.MustCompile("([a-z]+)(-|=)([0-9]+)?")
	labelIndex := 1
	opIndex := 2
	valueIndex := 3
	boxes := make([]Box, 256, 256)
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			continue
		}
		commands := strings.Split(line, ",")
		for _, command := range commands {
			groups := cmdPattern.FindStringSubmatch(command)
			label := groups[labelIndex]
			boxIndex := hash(label)
			op := groups[opIndex]
			if op == "-" {
				boxes[boxIndex].remove(label)
			} else {
				value, err := strconv.Atoi(groups[valueIndex])
				if err != nil {
					return -1, err
				}
				boxes[boxIndex].set(label, value)
			}
			//fmt.Printf("After \"%s\":\n", command)
			//printBoxes(boxes)
		}
	}
	total := 0
	for i := 0; i < len(boxes); i += 1 {
		total += boxes[i].total() * (i + 1)
	}

	return total, nil
}

func main() {
	if len(os.Args) > 1 {
		total, err := processInput(os.Args[1])
		if err == nil {
			fmt.Printf("Total: %d\n", total)
		} else {
			fmt.Printf("Problem processing input %s: %v\n", os.Args[1], err)
			os.Exit(-1)
		}
	} else {
		fmt.Printf("Missing required input argument\n")
		os.Exit(-1)
	}
}
