package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var forwardLookupTable = map[string]string{
	"0":     "0",
	"zero":  "0",
	"1":     "1",
	"one":   "1",
	"2":     "2",
	"two":   "2",
	"3":     "3",
	"three": "3",
	"4":     "4",
	"four":  "4",
	"5":     "5",
	"five":  "5",
	"6":     "6",
	"six":   "6",
	"7":     "7",
	"seven": "7",
	"8":     "8",
	"eight": "8",
	"9":     "9",
	"nine":  "9",
}

func reverse(s string) string {
	chars := []rune(s)
	for i, j := 0, len(chars)-1; i < j; i, j = i+1, j-1 {
		chars[i], chars[j] = chars[j], chars[i]
	}
	return string(chars)
}

func reverseKeys(m map[string]string) map[string]string {
	rval := make(map[string]string)
	for k, v := range m {
		rval[reverse(k)] = v
	}
	return rval
}

var backwardLookupTable = reverseKeys(forwardLookupTable)

func createRegexFromKeys(m map[string]string) *regexp.Regexp {
	rval := ""
	for k := range m {
		if rval == "" {
			rval = k
		} else {
			rval += "|" + k
		}
	}
	return regexp.MustCompile(rval)
}

var forwardRegex = createRegexFromKeys(forwardLookupTable)
var backwardRegex = createRegexFromKeys(backwardLookupTable)

func processLine(l string) int {
	l = strings.ToLower(l)
	first := forwardRegex.FindString(l)
	last := backwardRegex.FindString(reverse(l))
	if first != "" && last != "" {
		first := forwardLookupTable[first]
		last := backwardLookupTable[last]
		num, _ := strconv.Atoi(first + last)
		return num
	} else {
		return 0
	}
}

func processInput(f string) (int, error) {
	inputFile, err := os.Open(f)
	if err != nil {
		return -1, fmt.Errorf("Problem reading file at %s %w", f, err)
	}
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)
	total := 0
	for scanner.Scan() {
		total += processLine(scanner.Text())
	}

	return total, nil
}

func main() {
	if len(os.Args) > 1 {
		total, err := processInput(os.Args[1])
		if err == nil {
			fmt.Printf("Total: %d\n", total)
		} else {
			fmt.Printf("Problem processing input %s: %v", os.Args[1], err)
			os.Exit(-1)
		}
	} else {
		fmt.Printf("Missing required input argument\n")
		os.Exit(-1)
	}
}
