package main

import (
	"bufio"
	"fmt"
	"os"
)

type Grid struct {
	tiles  []rune
	width  int
	height int
}

func rollUp(g Grid, x, y int) int {
	prevIndex := (y * g.width) + x
	for j := y - 1; j >= 0; j -= 1 {
		index := (j * g.width) + x
		if g.tiles[index] == '.' {
			g.tiles[index], g.tiles[prevIndex] = g.tiles[prevIndex], g.tiles[index]
			prevIndex = index
		} else {
			return g.height - (j + 1)
		}
	}
	return g.height
}

func processGrid(g Grid) int {
	total := 0
	for i := 0; i < g.width; i += 1 {
		if g.tiles[i] == 'O' {
			total += g.height
		}
	}
	for y := 1; y < g.height; y += 1 {
		for x := 0; x < g.width; x += 1 {
			index := (y * g.width) + x
			if g.tiles[index] == 'O' {
				total += rollUp(g, x, y)
			}
		}
	}
	return total
}

func processInput(f string) (int, error) {
	inputFile, err := os.Open(f)
	if err != nil {
		return -1, fmt.Errorf("Problem reading file at %s %w", f, err)
	}
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)
	var grid Grid
	for scanner.Scan() {
		line := scanner.Text()
		if line != "" {
			grid.width = len(line)
			grid.height += 1
			grid.tiles = append(grid.tiles, []rune(line)...)
		}
	}
	total := processGrid(grid)
	return total, nil
}

func main() {
	if len(os.Args) > 1 {
		total, err := processInput(os.Args[1])
		if err == nil {
			fmt.Printf("Total: %d\n", total)
		} else {
			fmt.Printf("Problem processing input %s: %v\n", os.Args[1], err)
			os.Exit(-1)
		}
	} else {
		fmt.Printf("Missing required input argument\n")
		os.Exit(-1)
	}
}
