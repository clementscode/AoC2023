package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var gameregex = regexp.MustCompile("game ([0-9]+)")
var colorregex = regexp.MustCompile("([0-9]+)\\s+([a-z]+)")

func parseGameNum(s string) (int, error) {
	submatches := gameregex.FindSubmatch([]byte(s))
	if len(submatches) != 2 {
		return -1, fmt.Errorf("Invalid game num string %s", s)
	}
	return strconv.Atoi(string(submatches[1]))
}

func parseRound(s string) (map[string]int, error) {
	colors := strings.Split(s, ",")
	counts := make(map[string]int)
	for _, color := range colors {
		submatches := colorregex.FindSubmatch([]byte(color))
		num, err := strconv.Atoi(string(submatches[1]))
		if err != nil {
			return counts, fmt.Errorf("Invalid color count %s %w", color, err)
		}
		counts[string(submatches[2])] = num
	}
	return counts, nil
}

func processGame(game string, bag map[string]int) (int, bool, error) {
	game = strings.ToLower(game)
	parts := strings.Split(game, ":")
	if len(parts) != 2 {
		return -1, false, fmt.Errorf("Game line has no colon '%s'", game)
	}
	num, err := parseGameNum(parts[0])
	if err != nil {
		return -1, false, err
	}
	rounds := strings.Split(parts[1], ";")
	for _, round := range rounds {
		counts, err := parseRound(round)
		if err != nil {
			return -1, false, err
		}
		for k, v := range counts {
			max, ok := bag[k]
			if !ok || v > max {
				return num, false, nil
			}
		}
	}
	return num, true, nil
}

func getPower(game string) (int, error) {
	game = strings.ToLower(game)
	parts := strings.Split(game, ":")
	if len(parts) != 2 {
		return -1, fmt.Errorf("Game line has no colon '%s'", game)
	}
	maxes := make(map[string]int)
	rounds := strings.Split(parts[1], ";")
	for _, round := range rounds {
		counts, err := parseRound(round)
		if err != nil {
			return -1, err
		}
		for k, v := range counts {
			max, ok := maxes[k]
			if !ok || v > max {
				maxes[k] = v
			}
		}
	}
	power := 1
	for _, v := range maxes {
		power *= v
	}
	return power, nil

}

func processInput(gamefile string, bag map[string]int, powers bool) (int, error) {
	inputFile, err := os.Open(gamefile)
	if err != nil {
		return -1, fmt.Errorf("Problem reading file at %s %w", gamefile, err)
	}
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)
	total := 0
	line := 0
	for scanner.Scan() {
		if powers {
			num, err := getPower(scanner.Text())
			if err != nil {
				return total, fmt.Errorf("Error on line %d %w", line, err)
			}
			total += num
		} else {
			num, possible, err := processGame(scanner.Text(), bag)
			if err != nil {
				return total, fmt.Errorf("Error on line %d %w", line, err)
			}
			if possible {
				total += num
			}
		}
		line += 1
	}

	return total, nil
}

func main() {
	var gamefile string
	var bagjson string
	var powers bool
	bag := make(map[string]int)
	flag.StringVar(&gamefile, "gamefile", "", "input file")
	flag.StringVar(&bagjson, "bag", "", "bag contents as json")
	flag.BoolVar(&powers, "powers", false, "enable part 2")
	flag.Parse()
	if gamefile == "" {
		fmt.Printf("Missing gamefile argument\n")
		os.Exit(-1)
	}
	if !powers {
		if bagjson == "" {
			fmt.Printf("Missing bag argument\n")
			os.Exit(-1)
		}
		err := json.Unmarshal([]byte(bagjson), &bag)
		if err != nil {
			fmt.Printf("Invalid bag json %s %v\n", bagjson, err)
			os.Exit(-1)
		}
	}
	total, err := processInput(gamefile, bag, powers)
	if err == nil {
		fmt.Printf("Total: %d\n", total)
	} else {
		fmt.Printf("Problem processing input %s: %v", os.Args[1], err)
		os.Exit(-1)
	}
}
