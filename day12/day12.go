package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Config struct {
	pattern []rune
	groups  []int
}

func (c Config) getArrangements(ch chan int) {
	// fmt.Printf("%s\n", string(c.pattern))
	var subs []string
	subs = append(subs, "")
	for i := 0; i < len(c.groups); i += 1 {
		var newSubs []string
		for _, sub := range subs {
			newSubs = append(newSubs, c.getSubArrangements(i, sub)...)
		}
		subs = newSubs
	}
	/*
		fmt.Printf("%d\n", len(subs))
		fmt.Println()
	*/
	ch <- len(subs)
}

func (c Config) getReserve(group int) int {
	total := 0
	count := 0
	for i := group + 1; i < len(c.groups); i += 1 {
		total += c.groups[i]
		count += 1
	}
	if count > 0 {
		// count - 1 is the buffers between the groups
		return total + (count - 1)
	} else {
		return total
	}
}

func (c Config) fitsPattern(i int, r rune) bool {
	return c.pattern[i] == r || c.pattern[i] == '?'
}

func (c Config) writeRune(start, count int, r rune, sb *strings.Builder) bool {
	for i := 0; i < count; i += 1 {
		if !c.fitsPattern(start+i, r) {
			return false
		}
		sb.WriteRune(r)
	}
	return true
}

func (c Config) getSubArrangements(group int, base string) (results []string) {
	minStart := len(base)
	// start with one less than the remaining groups for the dots between
	reserve := c.getReserve(group)
	maxStart := len(c.pattern) - reserve - c.groups[group]
	dots := 0
	for i := minStart; i <= maxStart; i, dots = i+1, dots+1 {
		var sb strings.Builder
		sb.WriteString(base)
		if !c.writeRune(minStart, dots, '.', &sb) {
			continue
		}
		if !c.writeRune(i, c.groups[group], '#', &sb) {
			continue
		}
		endPad := 1
		if group == len(c.groups)-1 {
			endPad = len(c.pattern) - sb.Len()
		}
		if !c.writeRune(i+c.groups[group], endPad, '.', &sb) {
			continue
		}
		result := sb.String()
		/*
			if len(result) == len(c.pattern) {
				fmt.Printf("%s\n", result)
			}
		*/
		results = append(results, result)
	}
	return
}

func parseLine(l string) (c Config, err error) {
	parts := strings.Split(l, " ")
	if len(parts) != 2 {
		return c, fmt.Errorf("Invalid line %s\n", l)
	}
	var sb strings.Builder
	pattern := parts[0]
	sb.WriteString(pattern)
	for i := 1; i < 5; i += 1 {
		sb.WriteRune('?')
		sb.WriteString(pattern)

	}
	c.pattern = []rune(sb.String())
	numList := strings.Split(parts[1], ",")
	for i := 0; i < 5; i += 1 {
		for _, numStr := range numList {
			num, err := strconv.Atoi(numStr)
			if err != nil {
				return c, err
			}
			c.groups = append(c.groups, num)
		}
	}
	return
}

func processInput(f string) (int, error) {
	inputFile, err := os.Open(f)
	if err != nil {
		return -1, fmt.Errorf("Problem reading file at %s %w", f, err)
	}
	defer inputFile.Close()
	ch := make(chan int)
	scanner := bufio.NewScanner(inputFile)
	var count int
	for count = 0; scanner.Scan(); count += 1 {
		line := scanner.Text()
		if line == "" {
			continue
		}
		c, err := parseLine(line)
		if err != nil {
			return -1, err
		}
		go c.getArrangements(ch)
	}
	total := 0
	for i := 0; i < count; i += 1 {
		total += <-ch
	}
	return total, nil
}

func main() {
	if len(os.Args) > 1 {
		total, err := processInput(os.Args[1])
		if err == nil {
			fmt.Printf("Total: %d\n", total)
		} else {
			fmt.Printf("Problem processing input %s: %v\n", os.Args[1], err)
			os.Exit(-1)
		}
	} else {
		fmt.Printf("Missing required input argument\n")
		os.Exit(-1)
	}
}
