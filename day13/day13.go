package main

import (
	"bufio"
	"fmt"
	"os"
)

type Grid struct {
	tiles  []rune
	width  int
	height int
}

func (g *Grid) reset() {
	g.tiles = nil
	g.width = 0
	g.height = 0
}

func atRowReflection(y int, grid Grid) bool {
	revDist := y
	fwdDist := grid.height - y
	distLimit := revDist
	if revDist > fwdDist {
		distLimit = fwdDist
	}
	for offset := 1; offset <= distLimit; offset += 1 {
		index1 := y - offset
		index2 := y + (offset - 1)
		if !rowsEqual(index1, index2, grid) {
			return false
		}
	}
	return true
}

func rowsEqual(i, j int, grid Grid) bool {
	start1 := i * grid.width
	start2 := j * grid.width
	for n := 0; n < grid.width; n += 1 {
		if grid.tiles[start1+n] != grid.tiles[start2+n] {
			return false
		}
	}
	return true
}

func atColReflection(x int, grid Grid) bool {
	revDist := x
	fwdDist := grid.width - x
	distLimit := revDist
	if revDist > fwdDist {
		distLimit = fwdDist
	}
	for offset := 1; offset <= distLimit; offset += 1 {
		index1 := x - offset
		index2 := x + (offset - 1)
		if !colsEqual(index1, index2, grid) {
			return false
		}
	}
	return true
}

func colsEqual(i, j int, grid Grid) bool {
	for n := 0; n < grid.height; n += 1 {
		index1 := (n * grid.width) + i
		index2 := (n * grid.width) + j
		if grid.tiles[index1] != grid.tiles[index2] {
			return false
		}
	}
	return true
}

func processGrid(grid Grid) (total int) {
	for y := 1; y < grid.height; y += 1 {
		if atRowReflection(y, grid) {
			total += 100 * y
			break
		}
	}
	for x := 1; x < grid.width; x += 1 {
		if atColReflection(x, grid) {
			total += x
			break
		}
	}
	return
}

func processInput(f string) (int, error) {
	inputFile, err := os.Open(f)
	if err != nil {
		return -1, fmt.Errorf("Problem reading file at %s %w", f, err)
	}
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)
	total := 0
	var grid Grid
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			if grid.tiles != nil {
				total += processGrid(grid)
				grid.reset()
			}
		} else {
			grid.width = len(line)
			grid.height += 1
			grid.tiles = append(grid.tiles, []rune(line)...)
		}
	}
	if grid.tiles != nil {
		total += processGrid(grid)
	}
	return total, nil
}

func main() {
	if len(os.Args) > 1 {
		total, err := processInput(os.Args[1])
		if err == nil {
			fmt.Printf("Total: %d\n", total)
		} else {
			fmt.Printf("Problem processing input %s: %v\n", os.Args[1], err)
			os.Exit(-1)
		}
	} else {
		fmt.Printf("Missing required input argument\n")
		os.Exit(-1)
	}
}
