package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

var directivePattern = regexp.MustCompile("(([a-zA-Z]+)-to-([a-zA-Z]+) )?(seeds|map):")
var typeGroup = 4
var inputGroup = 2
var outputGroup = 3

type dataRange struct {
	start int
	end   int
}

func (r dataRange) count() int {
	return r.end - r.start
}

func (r dataRange) String() string {
	return fmt.Sprintf("Start %d, Count %d, End %d\n", r.start, r.count(), r.end)
}

type mapping struct {
	inputStart  int
	outputStart int
	count       int
}

func (m mapping) String() string {
	var sb strings.Builder
	sb.WriteString(fmt.Sprintf("input start : %d, count: %d, input end: %d\n", m.inputStart, m.count, m.inputEnd()))
	sb.WriteString(fmt.Sprintf("output start: %d, count: %d, output end: %d", m.outputStart, m.count, m.outputEnd()))
	return sb.String()
}

func (m mapping) inputEnd() int {
	return m.inputStart + m.count
}

func (m mapping) outputEnd() int {
	return m.outputStart + m.count
}

func (m mapping) compare(r dataRange) int {
	startsBeforeOver := r.start < m.inputEnd()
	endsAfterBegins := r.end > m.inputStart
	if startsBeforeOver {
		if endsAfterBegins {
			return 0
		} else {
			return -1
		}
	} else {
		return 1
	}
}

type table struct {
	input    string
	output   string
	mappings []mapping
}

func (t table) String() string {
	var sb strings.Builder
	sb.WriteString(fmt.Sprintf("%s to %s\n", t.input, t.output))
	for _, m := range t.mappings {
		sb.WriteString(fmt.Sprintf("%s\n", m))
	}
	return sb.String()
}

func (t table) apply(inputRanges []dataRange) (outputRanges []dataRange) {
	for _, r := range inputRanges {
		for i, m := range t.mappings {
			rangeDiff := m.compare(r)
			if rangeDiff < 0 {
				// this mapping is after our range
				// still need to map a range before it
				// so create direct mapping
				outputRanges = append(outputRanges, r)
				break
			} else if rangeDiff == 0 {
				startDiff := r.start - m.inputStart
				if startDiff < 0 {
					// mapping didn't contain start, create direct
					// start is our current unhandled position
					rangeCount := m.inputStart - r.start
					outputRanges = append(outputRanges, dataRange{r.start, r.start + rangeCount})
					r.start = m.inputStart
					startDiff = 0
				}
				outRangeStart := m.outputStart + startDiff
				endDiff := m.inputEnd() - r.end
				if endDiff >= 0 {
					// will cover rest of range
					outRangeEnd := m.outputEnd() - endDiff
					outputRanges = append(outputRanges, dataRange{outRangeStart, outRangeEnd})
					break
				} else {
					// won't cover rest of range
					rangeCount := m.outputEnd() - outRangeStart
					outRangeEnd := outRangeStart + rangeCount
					outputRanges = append(outputRanges, dataRange{outRangeStart, outRangeEnd})
					r.start += rangeCount
				}
			} else if i+1 == len(t.mappings) {
				// we are at the end of the mappings
				// create direct mapping for rest
				outputRanges = append(outputRanges, r)
				break
			}
		}
	}
	sort.Slice(outputRanges, func(i, j int) bool { return outputRanges[i].start < outputRanges[j].start })
	return
}

func parseSeeds(line string) ([]dataRange, error) {
	parts := strings.Split(line, ":")
	if len(parts) != 2 {
		return nil, fmt.Errorf("Invalid seeds line %s", line)
	}
	var seeds []int
	values := strings.Split(strings.TrimSpace(parts[1]), " ")
	for _, val := range values {
		num, err := strconv.Atoi(val)
		if err != nil {
			return nil, fmt.Errorf("Invalid seeds line %s %w", line, err)
		}
		seeds = append(seeds, num)
	}
	if len(seeds)%2 != 0 {
		return nil, fmt.Errorf("Seeds must be even number %s", line)
	}
	var seedRanges []dataRange
	for i := 0; i < len(seeds); i += 2 {
		start := seeds[i]
		count := seeds[i+1]
		seedRanges = append(seedRanges, dataRange{start, start + count})
	}
	sort.Slice(seedRanges, func(i, j int) bool { return seedRanges[i].start < seedRanges[j].start })
	return seedRanges, nil
}

func parseTable(scanner *bufio.Scanner, input, output string) (t table, err error) {
	t.input = input
	t.output = output
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		if line == "" {
			break
		}
		parts := strings.Split(line, " ")
		if len(parts) != 3 {
			err = fmt.Errorf("Invalid table entry %s for %s-to-%s", line, input, output)
			return
		}
		var num int
		var nums []int
		for i := 0; i < 3; i += 1 {
			num, err = strconv.Atoi(parts[i])
			if err != nil {
				err = fmt.Errorf("Invalid table entry %s for %s-to-%s %w", line, input, output, err)
				return
			}
			nums = append(nums, num)
		}
		outputIndex := nums[0]
		inputIndex := nums[1]
		count := nums[2]
		t.mappings = append(t.mappings, mapping{inputIndex, outputIndex, count})
	}
	sort.Slice(t.mappings, func(i, j int) bool { return t.mappings[i].inputStart < t.mappings[j].inputStart })
	return
}

func getMinLocation(seedRanges []dataRange, tables map[string]table) int {
	ok := true
	inputType := "seed"
	ranges := seedRanges
	table, ok := tables[inputType]
	for ok {
		/*
			fmt.Printf("Input ranges:\n")
			for _, r := range ranges {
				fmt.Printf("%s", r.String())
			}
			fmt.Printf("Table:\n")
			fmt.Printf("%s", table.String())
		*/
		ranges = table.apply(ranges)
		/*
			fmt.Printf("Output ranges:\n")
			for _, r := range ranges {
				fmt.Printf("%s", r.String())
			}
			fmt.Println()
		*/
		table, ok = tables[table.output]
	}
	return ranges[0].start
}

func processInput(f string) (int, error) {
	inputFile, err := os.Open(f)
	if err != nil {
		return -1, fmt.Errorf("Problem reading file at %s %w", f, err)
	}
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)
	tables := make(map[string]table)
	var seeds []dataRange
	for scanner.Scan() {
		line := scanner.Text()
		matches := directivePattern.FindStringSubmatch(line)
		if matches == nil {
			continue
		}
		t := matches[typeGroup]
		if t == "seeds" {
			seeds, err = parseSeeds(line)
			if err != nil {
				return -1, fmt.Errorf("Problem parsing seeds %w", err)
			}
		} else if t == "map" {
			input := matches[inputGroup]
			output := matches[outputGroup]
			table, err := parseTable(scanner, input, output)
			if err != nil {
				return -1, fmt.Errorf("Problem parsing table %w", err)
			}
			tables[input] = table
		}
	}

	if seeds == nil {
		return -1, fmt.Errorf("Didn't find seeds")
	}

	minLocation := getMinLocation(seeds, tables)

	return minLocation, nil
}

func main() {
	if len(os.Args) > 1 {
		total, err := processInput(os.Args[1])
		if err == nil {
			fmt.Printf("Min Location: %d\n", total)
		} else {
			fmt.Printf("Problem processing input %s: %v\n", os.Args[1], err)
			os.Exit(-1)
		}
	} else {
		fmt.Printf("Missing required input argument\n")
		os.Exit(-1)
	}
}
