package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

const (
	NORTH = iota
	SOUTH
	EAST
	WEST
)

var EMPTY_WEIGHT = 1000000

var DIRS = []int{NORTH, SOUTH, EAST, WEST}
var DIR_STR = []string{"NORTH", "SOUTH", "EAST", "WEST"}

var REVERSE = map[int]int{
	NORTH: SOUTH,
	SOUTH: NORTH,
	EAST:  WEST,
	WEST:  EAST,
}

type Coord struct {
	x int
	y int
}

func (c Coord) String() string {
	return fmt.Sprintf("(%d,%d)", c.x, c.y)
}

type Vector struct {
	from Coord
	to   Coord
	dir  int
}

func (v Vector) String() string {
	return fmt.Sprintf("from %v to %v %s", v.from, v.to, DIR_STR[v.dir])
}

type Node struct {
	galaxy int
	edges  [4]int
}

func walk(c1 Coord, dir int) (c2 Coord) {
	c2 = c1
	switch dir {
	case NORTH:
		c2.y -= 1
	case SOUTH:
		c2.y += 1
	case EAST:
		c2.x += 1
	case WEST:
		c2.x -= 1
	}
	return
}

type Grid struct {
	nodes    []Node
	width    int
	height   int
	galaxies []Coord
}

func (g *Grid) getNode(c Coord) (n Node, err error) {
	if c.x < 0 || c.x >= g.width || c.y < 0 || c.y >= g.height {
		err = fmt.Errorf("Invalid coord %d,%d", c.x, c.y)
		return
	}
	index := (g.width * c.y) + c.x
	return g.nodes[index], nil
}

func (g *Grid) addRow(line string) error {
	width := len(line)
	if g.width == 0 {
		g.width = width
	} else if g.width != width {
		return fmt.Errorf("Provided line width %d does not match grid width %d", width, g.width)
	}
	numGalaxies := 0
	for x, c := range line {
		n := Node{}
		switch c {
		case '#':
			g.galaxies = append(g.galaxies, Coord{x, g.height})
			n.galaxy = len(g.galaxies)
			numGalaxies += 1
		case '.':
			n.galaxy = -1
		default:
			return fmt.Errorf("Unexpected character %s", string(c))
		}
		if x == 0 {
			n.edges[WEST] = -1
			n.edges[EAST] = 1
		} else if x == g.width-1 {
			n.edges[WEST] = 1
			n.edges[EAST] = -1
		} else {
			n.edges[WEST] = 1
			n.edges[EAST] = 1
		}
		// default to assuming this is the last row
		// will be updated on next line if there is one
		n.edges[SOUTH] = -1
		g.nodes = append(g.nodes, n)
	}
	edgeWeight := 1
	if numGalaxies == 0 {
		edgeWeight = EMPTY_WEIGHT
	}
	if g.height == 0 {
		rowStart := g.height * g.width
		for x := 0; x < g.width; x += 1 {
			g.nodes[rowStart+x].edges[NORTH] = -1
		}
	} else {
		rowStart := g.height * g.width
		prevRowStart := (g.height - 1) * g.width
		for x := 0; x < g.width; x += 1 {
			g.nodes[rowStart+x].edges[NORTH] = edgeWeight
			g.nodes[prevRowStart+x].edges[SOUTH] = edgeWeight
		}
	}
	g.height += 1
	return nil
}

func (g *Grid) resolveEmptyColumns() error {
	columns := make([]bool, g.width, g.width)
	for _, c := range g.galaxies {
		columns[c.x] = true
	}
	for x := 0; x < len(columns); x += 1 {
		if !columns[x] {
			prevX := x - 1
			if prevX < 0 {
				return fmt.Errorf("Unexpected empty first column")
			}
			for y := 0; y < g.height; y += 1 {
				index := (y * g.width) + x
				prevIndex := (y * g.width) + prevX
				g.nodes[index].edges[WEST] = EMPTY_WEIGHT
				g.nodes[prevIndex].edges[EAST] = EMPTY_WEIGHT
			}
		}
	}
	return nil
}

func (g *Grid) gridDist(start, end Coord) int {
	startX, startY := start.x, start.y
	endX, endY := end.x, end.y
	if startX > endX {
		startX, endX = endX, startX
	}
	if startY > endY {
		startY, endY = endY, startY
	}
	xDist := 0
	for x := startX; x < endX; x += 1 {
		xDist += g.nodes[x].edges[EAST]
	}
	yDist := 0
	for y := startY; y < endY; y += 1 {
		yDist += g.nodes[y*g.width].edges[SOUTH]
	}
	return xDist + yDist
}

func (g *Grid) String() string {
	var sb strings.Builder
	for y := 0; y < g.height; y += 1 {
		start := y * g.width
		for x := 0; x < g.width; x += 1 {
			n := g.nodes[start+x]
			if n.galaxy > 0 {
				sb.WriteString(fmt.Sprintf("%d", n.galaxy))
			} else {
				sb.WriteRune('.')
			}
			if n.edges[WEST] > 1 {
				sb.WriteRune('.')
			}
		}
		sb.WriteRune('\n')
		if g.nodes[start].edges[SOUTH] > 1 {
			for x := 0; x < g.width; x += 1 {
				sb.WriteRune('.')
			}
			sb.WriteRune('\n')
		}
	}
	return sb.String()
}

type AStar struct {
	heap   []Coord
	gScore map[Coord]int
	fScore map[Coord]int
}

func (a *AStar) update(c Coord) {
	var i int
	for i = 0; i < len(a.heap); i += 1 {
		if a.heap[i] == c {
			break
		}
	}
	if i == len(a.heap) {
		a.heap = append(a.heap, c)
	}
	f := a.fScore[c]
	for i > 0 {
		parentIndex := (i - 1) / 2
		parent := a.heap[parentIndex]
		if a.fScore[parent] > f {
			a.heap[i], a.heap[parentIndex] = a.heap[parentIndex], a.heap[i]
		} else {
			break
		}
		i = parentIndex
	}
}

func (a *AStar) popHeap() (c Coord, ok bool) {
	if len(a.heap) == 0 {
		return c, false
	}
	c = a.heap[0]
	lastIndex := len(a.heap) - 1
	a.heap[0] = a.heap[lastIndex]
	a.heap = a.heap[:lastIndex]
	parent := 0
	for parent < lastIndex {
		child, exists := a.getSmallestChild(parent)
		if !exists {
			break
		}
		if a.fScore[a.heap[parent]] > a.fScore[a.heap[child]] {
			a.heap[parent], a.heap[child] = a.heap[child], a.heap[parent]
		}
		parent = child
	}
	return c, true
}

func (a *AStar) getSmallestChild(parentIndex int) (int, bool) {
	childIndex1 := (parentIndex * 2) + 1
	size := len(a.heap)
	if size > childIndex1 {
		child1 := a.heap[childIndex1]
		childIndex2 := childIndex1 + 1
		if size > childIndex2 && a.fScore[child1] > a.fScore[a.heap[childIndex2]] {
			return childIndex2, true
		} else {
			return childIndex1, true
		}
	}
	return parentIndex, false
}

func (a *AStar) getMinDist(grid *Grid, start, end Coord, ch chan int) {
	a.heap = nil
	a.fScore = make(map[Coord]int)
	a.gScore = make(map[Coord]int)
	a.gScore[start] = 0
	a.fScore[start] = grid.gridDist(start, end)
	a.update(start)
	for len(a.heap) > 0 {
		curr, _ := a.popHeap()
		if curr == end {
			ch <- a.gScore[curr]
			return
		}
		n, err := grid.getNode(curr)
		if err != nil {
			fmt.Printf("Error from %s to %s %v\n", start.String(), end.String(), err)
			return
		}
		for _, dir := range DIRS {
			weight := n.edges[dir]
			if weight < 0 {
				continue
			}
			neighbor := walk(curr, dir)
			g := a.gScore[curr] + weight
			existing, ok := a.gScore[neighbor]
			if !ok || g < existing {
				a.gScore[neighbor] = g
				a.fScore[neighbor] = g + grid.gridDist(neighbor, end)
				a.update(neighbor)
			}
		}
	}
	fmt.Printf("No path from %s to %s\n", start.String(), end.String())
}

func processInput(f string) (int, error) {
	inputFile, err := os.Open(f)
	if err != nil {
		return -1, fmt.Errorf("Problem reading file at %s %w", f, err)
	}
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)
	var g Grid
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			continue
		}
		err := g.addRow(scanner.Text())
		if err != nil {
			return -1, err
		}
	}
	err = g.resolveEmptyColumns()
	if err != nil {
		return -1, err
	}
	total := 0
	for i := 0; i < len(g.galaxies); i += 1 {
		start := g.galaxies[i]
		for j := i + 1; j < len(g.galaxies); j += 1 {
			end := g.galaxies[j]
			total += g.gridDist(start, end)
		}
	}
	return total, nil
}

func main() {
	if len(os.Args) > 1 {
		total, err := processInput(os.Args[1])
		if err == nil {
			fmt.Printf("Total: %d\n", total)
		} else {
			fmt.Printf("Problem processing input %s: %v\n", os.Args[1], err)
			os.Exit(-1)
		}
	} else {
		fmt.Printf("Missing required input argument\n")
		os.Exit(-1)
	}
}
