package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

var lookup = []rune{'J', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'Q', 'K', 'A'}
var tens = []int{1, 10, 100, 1000, 10000, 100000}

type Hand struct {
	rank  int
	cards []int
	wager int
}

func getValue(c rune) int {
	for i := 0; i < len(lookup); i += 1 {
		if lookup[i] == c {
			return i
		}
	}
	return -1
}

func resolveJokers(cards []rune) []rune {
	bins := make(map[rune][]int)
	for index, c := range cards {
		indexes := append(bins[c], index)
		bins[c] = indexes
	}
	jokers, ok := bins['J']
	if !ok {
		return cards
	}
	if len(jokers) == 5 {
		return []rune("AAAAA")
	}
	var countOrder []rune
	var valueOrder []rune
	for k := range bins {
		if k == 'J' {
			continue
		}
		countOrder = append(countOrder, k)
		valueOrder = append(valueOrder, k)
	}
	sort.Slice(countOrder, func(i, j int) bool {
		left := len(bins[countOrder[i]])
		right := len(bins[countOrder[j]])
		return left > right
	})
	sort.Slice(valueOrder, func(i, j int) bool {
		return getValue(valueOrder[i]) > getValue(valueOrder[j])
	})
	mostCard := countOrder[0]
	mostIdxs := bins[mostCard]
	if len(mostIdxs) == 4 || len(mostIdxs) == 3 {
		for j := 0; j < len(jokers); j += 1 {
			cards[jokers[j]] = cards[mostIdxs[0]]
		}
	} else if len(mostIdxs) == 2 && len(countOrder) > 1 {
		highValue := mostCard
		second := countOrder[1]
		secondIdxs := bins[second]
		if len(secondIdxs) == 2 && getValue(second) > getValue(mostCard) {
			highValue = second
		}
		for j := 0; j < len(jokers); j += 1 {
			cards[jokers[j]] = highValue
		}
	} else {
		highValue := valueOrder[0]
		for j := 0; j < len(jokers); j += 1 {
			cards[jokers[j]] = highValue
		}
	}
	return cards
}

func parseHand(line string) (h Hand, err error) {
	parts := strings.Split(line, " ")
	if len(parts) != 2 {
		return h, fmt.Errorf("Invalid line %s", line)
	}
	var counts = make(map[rune]int)
	cards := parts[0]
	effectiveHand := resolveJokers([]rune(parts[0]))
	for j, c := range cards {
		found := false
		for i := 0; i < len(lookup); i += 1 {
			if lookup[i] == c {
				h.cards = append(h.cards, i)
				effectiveCard := effectiveHand[j]
				count, ok := counts[effectiveCard]
				if ok {
					counts[effectiveCard] = count + 1
				} else {
					counts[effectiveCard] = 1
				}
				found = true
				break
			}
		}
		if !found {
			return h, fmt.Errorf("Invalid cards in %s", line)
		}
	}
	for _, count := range counts {
		h.rank += tens[count]
	}
	h.wager, err = strconv.Atoi(parts[1])
	return h, err
}

func (h Hand) compare(other Hand) int {
	diff := h.rank - other.rank
	if diff != 0 {
		return diff
	}
	for i := 0; i < len(h.cards); i += 1 {
		diff = h.cards[i] - other.cards[i]
		if diff != 0 {
			return diff
		}
	}
	return diff
}

type Heap struct {
	hands []Hand
}

func (h *Heap) push(hand Hand) {
	h.hands = append(h.hands, hand)
	h.perkUp()
}

func (h *Heap) pop() (hand Hand, ok bool) {
	if len(h.hands) == 0 {
		return hand, false
	}
	hand = h.hands[0]
	h.swap(0, len(h.hands)-1)
	h.hands = h.hands[:len(h.hands)-1]
	h.perkDown()
	return hand, true
}

func (h *Heap) swap(i, j int) {
	h.hands[i], h.hands[j] = h.hands[j], h.hands[i]
}
func (h *Heap) perkUp() {
	for child := len(h.hands) - 1; child > 0; {
		parent := h.getParent(child)
		if h.hands[parent].compare(h.hands[child]) > 0 {
			h.swap(parent, child)
		} else {
			return
		}
		child = parent
	}
}

func (h *Heap) perkDown() {
	parent := 0
	for parent < len(h.hands) {
		child, exists := h.getSmallestChild(parent)
		if !exists {
			return
		}
		if h.hands[parent].compare(h.hands[child]) > 0 {
			h.swap(parent, child)
		}
		parent = child
	}
}

func (h *Heap) getParent(childIndex int) int {
	return (childIndex - 1) / 2
}

func (h *Heap) getSmallestChild(parentIndex int) (int, bool) {
	childIndex1 := (parentIndex * 2) + 1
	size := len(h.hands)
	if size > childIndex1 {
		child1 := h.hands[childIndex1]
		childIndex2 := childIndex1 + 1
		if size > childIndex2 && child1.compare(h.hands[childIndex2]) > 0 {
			return childIndex2, true
		} else {
			return childIndex1, true
		}
	}
	return parentIndex, false
}

func processInput(f string) (int, error) {
	inputFile, err := os.Open(f)
	if err != nil {
		return -1, fmt.Errorf("Problem reading file at %s %w", f, err)
	}
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)
	heap := Heap{nil}
	for scanner.Scan() {
		hand, err := parseHand(scanner.Text())
		if err != nil {
			return -1, err
		}
		heap.push(hand)
	}
	total := 0
	count := 0
	dupes := 0
	i := 1
	var prev Hand
	hand, ok := heap.pop()
	for ok {
		count += 1
		total += i * hand.wager
		if hand.compare(prev) != 0 {
			i += 1
		} else {
			dupes += 1
		}
		prev = hand
		hand, ok = heap.pop()
	}
	fmt.Printf("Count %d, dupes %d\n", count, dupes)
	return total, nil
}

func main() {
	if len(os.Args) > 1 {
		total, err := processInput(os.Args[1])
		if err == nil {
			fmt.Printf("Total: %d\n", total)
		} else {
			fmt.Printf("Problem processing input %s: %v\n", os.Args[1], err)
			os.Exit(-1)
		}
	} else {
		fmt.Printf("Missing required input argument\n")
		os.Exit(-1)
	}
}
