package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func parseNumList(value string) ([]int, error) {
	parts := strings.Split(value, " ")
	var results []int
	for _, nstr := range parts {
		if nstr != "" {
			n, err := strconv.Atoi(nstr)
			if err != nil {
				return nil, fmt.Errorf("Invalid num list %s %w", value, err)
			}
			results = append(results, n)
		}
	}
	return results, nil
}

func getNext(seq []int, ch chan int) {
	var rows [][]int
	rows = append(rows, seq)
	allZero := false
	for i := 0; !allZero; i += 1 {
		var row []int
		row, allZero = getDiffs(rows[i])
		rows = append(rows, row)
	}
	numRows := len(rows)
	firstColumn := make([]int, numRows, numRows)
	for i := numRows - 2; i >= 0; i -= 1 {
		row := rows[i]
		firstNum := row[0]
		nextFirstNum := firstColumn[i+1]
		firstColumn[i] = firstNum - nextFirstNum
	}
	ch <- firstColumn[0]
}

func getDiffs(seq []int) (diffs []int, allZero bool) {
	allZero = true
	for i := 1; i < len(seq); i += 1 {
		diff := seq[i] - seq[i-1]
		if diff != 0 {
			allZero = false
		}
		diffs = append(diffs, diff)
	}
	return
}

func processInput(f string) (int, error) {
	inputFile, err := os.Open(f)
	if err != nil {
		return -1, fmt.Errorf("Problem reading file at %s %w", f, err)
	}
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)
	ch := make(chan int)
	var count int
	for count = 0; scanner.Scan(); count += 1 {
		seq, err := parseNumList(scanner.Text())
		if err != nil {
			return -1, err
		}
		go getNext(seq, ch)
	}
	total := 0
	for i := 0; i < count; i += 1 {
		next := <-ch
		total += next
	}
	return total, nil
}

func main() {
	if len(os.Args) > 1 {
		total, err := processInput(os.Args[1])
		if err == nil {
			fmt.Printf("Total: %d\n", total)
		} else {
			fmt.Printf("Problem processing input %s: %v\n", os.Args[1], err)
			os.Exit(-1)
		}
	} else {
		fmt.Printf("Missing required input argument\n")
		os.Exit(-1)
	}
}
