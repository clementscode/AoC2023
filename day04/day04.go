package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func parseNumList(value string) ([]int, error) {
	parts := strings.Split(value, " ")
	var results []int
	for _, nstr := range parts {
		if nstr != "" {
			n, err := strconv.Atoi(nstr)
			if err != nil {
				return nil, fmt.Errorf("Invalid num list %s %w", value, err)
			}
			results = append(results, n)
		}
	}
	return results, nil
}

func inSlice(values []int, num int) bool {
	for _, value := range values {
		if value == num {
			return true
		}
	}
	return false
}

func processLine(l string) (int, error) {
	parts := strings.Split(l, ":")
	if len(parts) != 2 {
		return -1, fmt.Errorf("Line does not contain one colon %s", l)
	}
	parts = strings.Split(parts[1], "|")
	if len(parts) != 2 {
		return -1, fmt.Errorf("Line does not contain one pipe %s", l)
	}
	winners, err := parseNumList(parts[0])
	if err != nil {
		return -1, fmt.Errorf("Invalid winner list %w", err)
	}
	values, err := parseNumList(parts[1])
	if err != nil {
		return -1, fmt.Errorf("Invalid values list %w", err)
	}
	count := 0
	for _, v := range values {
		if inSlice(winners, v) {
			count += 1
		}
	}
	return count, nil
}

func incCardCount(counts []int, index int) []int {
	if len(counts) < index+1 {
		counts = append(counts, 1)
	} else {
		counts[index] += 1
	}
	return counts
}

func processInput(f string) (int, error) {
	inputFile, err := os.Open(f)
	if err != nil {
		return -1, fmt.Errorf("Problem reading file at %s %w", f, err)
	}
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)
	var counts []int
	index := 0
	for scanner.Scan() {
		counts = incCardCount(counts, index)
		card := scanner.Text()
		num, err := processLine(card)
		if err != nil {
			return -1, err
		}
		for i := 0; i < counts[index]; i += 1 {
			for n := 1; n <= num; n += 1 {
				counts = incCardCount(counts, index+n)
			}
		}
		index += 1
	}
	total := 0
	for _, count := range counts {
		total += count
	}

	return total, nil
}

func main() {
	if len(os.Args) > 1 {
		total, err := processInput(os.Args[1])
		if err == nil {
			fmt.Printf("Total: %d\n", total)
		} else {
			fmt.Printf("Problem processing input %s: %v\n", os.Args[1], err)
			os.Exit(-1)
		}
	} else {
		fmt.Printf("Missing required input argument\n")
		os.Exit(-1)
	}
}
