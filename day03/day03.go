package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func isDigit(c rune) bool {
	return c >= '0' && c <= '9'
}

type number struct {
	value int
	start int
	end   int
}

type parsedline struct {
	value []rune
	nums  []number
}

func (pl *parsedline) getAdjacentNums(i int) []number {
	var rval []number
	for _, num := range pl.nums {
		if num.start <= (i+1) && num.end >= i {
			rval = append(rval, num)
		}
	}
	return rval
}

func parseLine(line string) (*parsedline, error) {
	inNum := false
	var numstr []rune
	var nums []number
	for i, c := range line {
		if isDigit(c) {
			inNum = true
			numstr = append(numstr, c)
		} else {
			if inNum {
				num, err := strconv.Atoi(string(numstr))
				if err != nil {
					return nil, err
				}
				nums = append(nums, number{num, i - len(numstr), i})
			}
			inNum = false
			numstr = nil
		}
	}
	if inNum {
		num, err := strconv.Atoi(string(numstr))
		if err != nil {
			return nil, err
		}
		linelen := len(line)
		nums = append(nums, number{num, linelen - len(numstr), linelen})
	}
	return &parsedline{[]rune(line), nums}, nil

}

func processLine(prev, curr, next *parsedline) (int, error) {
	total := 0
	for i, c := range curr.value {
		if c == '*' {
			var nums []number
			if prev != nil {
				nums = prev.getAdjacentNums(i)
			}
			nums = append(nums, curr.getAdjacentNums(i)...)
			if next != nil {
				nums = append(nums, next.getAdjacentNums(i)...)
			}
			if len(nums) == 2 {
				product := nums[0].value * nums[1].value
				total += product
			}
		}
	}
	return total, nil
}

func processInput(f string) (int, error) {
	inputFile, err := os.Open(f)
	if err != nil {
		return -1, fmt.Errorf("Problem reading file at %s %w", f, err)
	}
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)
	total := 0
	var prev, curr, next *parsedline
	for scanner.Scan() {
		prev = curr
		curr = next
		next, err = parseLine(scanner.Text())
		if err != nil {
			return -1, err
		}
		if curr != nil {
			num, err := processLine(prev, curr, next)
			if err != nil {
				return -1, err
			}
			total += num
		}
	}
	if next != nil {
		num, err := processLine(curr, next, nil)
		if err != nil {
			return -1, err
		}
		total += num
	}

	return total, nil
}

func main() {
	if len(os.Args) > 1 {
		total, err := processInput(os.Args[1])
		if err == nil {
			fmt.Printf("Total: %d\n", total)
		} else {
			fmt.Printf("Problem processing input %s: %v\n", os.Args[1], err)
			os.Exit(-1)
		}
	} else {
		fmt.Printf("Missing required input argument\n")
		os.Exit(-1)
	}
}
