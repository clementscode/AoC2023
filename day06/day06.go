package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func parseNumList(value string) ([]int, error) {
	parts := strings.Split(value, " ")
	var results []int
	for _, nstr := range parts {
		if nstr != "" {
			n, err := strconv.Atoi(nstr)
			if err != nil {
				return nil, fmt.Errorf("Invalid num list %s %w", value, err)
			}
			results = append(results, n)
		}
	}
	return results, nil
}

func getDistance(holdTime, totalTime int) int {
	speed := holdTime
	runTime := totalTime - holdTime
	return speed * runTime
}

func processInput(f string) (int, error) {
	inputFile, err := os.Open(f)
	if err != nil {
		return -1, fmt.Errorf("Problem reading file at %s %w", f, err)
	}
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)
	inputMap := make(map[string][]int)
	for scanner.Scan() {
		parts := strings.Split(scanner.Text(), ":")
		id := strings.TrimSpace(parts[0])
		nums, err := parseNumList(parts[1])
		if err != nil {
			return -1, err
		}
		inputMap[id] = nums
	}
	total := 1
	times := inputMap["Time"]
	distances := inputMap["Distance"]
	for i := 0; i < len(times); i += 1 {
		t := times[i]
		d := distances[i]
		wins := 0
		for h := 0; h < t; h += 1 {
			attempt := getDistance(h, t)
			if attempt > d {
				wins += 1
			}
		}
		total *= wins
	}
	return total, nil
}

func main() {
	if len(os.Args) > 1 {
		total, err := processInput(os.Args[1])
		if err == nil {
			fmt.Printf("Total: %d\n", total)
		} else {
			fmt.Printf("Problem processing input %s: %v\n", os.Args[1], err)
			os.Exit(-1)
		}
	} else {
		fmt.Printf("Missing required input argument\n")
		os.Exit(-1)
	}
}
