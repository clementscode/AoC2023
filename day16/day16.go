package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type Direction int

const (
	NORTH Direction = iota
	SOUTH
	EAST
	WEST
)

var OFFSETS = [4][2]int{{0, -1}, {0, 1}, {1, 0}, {-1, 0}}

var NAMES = [4]string{"NORTH", "SOUTH", "EAST", "WEST"}

type Cell struct {
	icon    rune
	sources [4]bool
}

func (c Cell) litFrom(d Direction) bool {
	return c.sources[d]
}

func (c Cell) lit() bool {
	for _, lit := range c.sources {
		if lit {
			return true
		}
	}
	return false
}

func (c *Cell) light(d Direction) {
	c.sources[d] = true
}

func (c *Cell) reset() {
	for i := 0; i < len(c.sources); i += 1 {
		c.sources[i] = false
	}
}

type Grid struct {
	cells  []Cell
	width  int
	height int
}

func (g *Grid) addLine(l string) {
	l = strings.TrimSpace(l)
	g.width = len(l)
	g.height += 1
	for _, r := range l {
		g.cells = append(g.cells, Cell{icon: r})
	}
}

func (g Grid) countLit() int {
	total := 0
	for i := 0; i < len(g.cells); i += 1 {
		if g.cells[i].lit() {
			total += 1
		}
	}
	return total
}

func (g *Grid) reset() {
	for i := 0; i < len(g.cells); i += 1 {
		g.cells[i].reset()
	}
}

func (g Grid) validX(x int) bool {
	return x >= 0 && x < g.width
}

func (g Grid) validY(y int) bool {
	return y >= 0 && y < g.height
}

func (g Grid) shine(x, y int, dir Direction) {
	index := (y * g.width) + x
	if g.cells[index].litFrom(dir) {
		// already went down this path, stop
		return
	}
	// mark as lit from this direction
	g.cells[index].light(dir)
	x1, y1 := -1, -1
	var dir1 Direction
	x2, y2 := -1, -1
	var dir2 Direction
	switch g.cells[index].icon {
	case '.':
		// keep going
		x1, y1, dir1 = step(x, y, dir)
	case '|':
		if dir == NORTH || dir == SOUTH {
			x1, y1, dir1 = step(x, y, dir)
		} else {
			x1, y1, dir1 = step(x, y, NORTH)
			x2, y2, dir2 = step(x, y, SOUTH)
		}
	case '-':
		if dir == EAST || dir == WEST {
			x1, y1, dir1 = step(x, y, dir)
		} else {
			x1, y1, dir1 = step(x, y, EAST)
			x2, y2, dir2 = step(x, y, WEST)
		}
	case '/':
		if dir == NORTH {
			x1, y1, dir1 = step(x, y, EAST)
		} else if dir == SOUTH {
			x1, y1, dir1 = step(x, y, WEST)
		} else if dir == EAST {
			x1, y1, dir1 = step(x, y, NORTH)
		} else {
			x1, y1, dir1 = step(x, y, SOUTH)
		}
	case '\\':
		if dir == NORTH {
			x1, y1, dir1 = step(x, y, WEST)
		} else if dir == SOUTH {
			x1, y1, dir1 = step(x, y, EAST)
		} else if dir == EAST {
			x1, y1, dir1 = step(x, y, SOUTH)
		} else {
			x1, y1, dir1 = step(x, y, NORTH)
		}
	}
	if g.validX(x1) && g.validY(y1) {
		g.shine(x1, y1, dir1)
	}
	if g.validX(x2) && g.validY(y2) {
		g.shine(x2, y2, dir2)
	}
}

func step(x, y int, dir Direction) (int, int, Direction) {
	offset := OFFSETS[dir]
	return x + offset[0], y + offset[1], dir
}

func processInput(f string) (int, error) {
	inputFile, err := os.Open(f)
	if err != nil {
		return -1, fmt.Errorf("Problem reading file at %s %w", f, err)
	}
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)
	var g Grid
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			continue
		}
		g.addLine(line)
	}
	count := 0
	max := 0
	for x := 0; x < g.width; x += 1 {
		g.shine(x, 0, SOUTH)
		count := g.countLit()
		if count > max {
			max = count
		}
		g.reset()
		g.shine(x, g.height-1, NORTH)
		count = g.countLit()
		if count > max {
			max = count
		}
		g.reset()
	}
	for y := 0; y < g.height; y += 1 {
		g.shine(0, y, EAST)
		count = g.countLit()
		if count > max {
			max = count
		}
		g.reset()
		g.shine(g.width-1, y, WEST)
		count = g.countLit()
		if count > max {
			max = count
		}
		g.reset()
	}
	return max, nil
}

func main() {
	if len(os.Args) > 1 {
		total, err := processInput(os.Args[1])
		if err == nil {
			fmt.Printf("Total: %d\n", total)
		} else {
			fmt.Printf("Problem processing input %s: %v\n", os.Args[1], err)
			os.Exit(-1)
		}
	} else {
		fmt.Printf("Missing required input argument\n")
		os.Exit(-1)
	}
}
