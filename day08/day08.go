package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strings"
)

var nodePattern = regexp.MustCompile("([0-9A-Z]+) = \\(([0-9A-Z]+), ([0-9A-Z]+)\\)")
var nameGroup = 1
var leftGroup = 2
var rightGroup = 3

type Node struct {
	name      string
	leftName  string
	left      *Node
	rightName string
	right     *Node
}

func parseNode(line string) (n *Node, err error) {
	groups := nodePattern.FindStringSubmatch(line)
	if len(groups) < 3 {
		return nil, fmt.Errorf("Invalid node %s", line)
	}
	name := groups[nameGroup]
	left := groups[leftGroup]
	right := groups[rightGroup]
	return &Node{name, left, nil, right, nil}, nil
}

func walkNode(n *Node, dirs string, ch chan int) {
	step := 0
	found := false
	var count int
	for count = 0; !found; count += 1 {
		if dirs[step] == 'L' {
			n = n.left
		} else {
			n = n.right
		}
		if []rune(n.name)[2] == 'Z' {
			found = true
		}
		step += 1
		if step >= len(dirs) {
			step = 0
		}
	}
	ch <- count
}

func processInput(f string) (int, error) {
	inputFile, err := os.Open(f)
	if err != nil {
		return -1, fmt.Errorf("Problem reading file at %s %w", f, err)
	}
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)
	if !scanner.Scan() {
		return -1, fmt.Errorf("Empty input")
	}
	dirs := scanner.Text()
	nodes := make(map[string]*Node)
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		if line == "" {
			continue
		}
		node, err := parseNode(line)
		if err != nil {
			return -1, fmt.Errorf("Problem parsing node")
		}
		nodes[node.name] = node
	}
	ok := true
	for _, node := range nodes {
		node.left, ok = nodes[node.leftName]
		if !ok {
			return -1, fmt.Errorf("Unable to find node %s", node.leftName)
		}
		node.right, ok = nodes[node.rightName]
		if !ok {
			return -1, fmt.Errorf("Unable to find node %s", node.leftName)
		}
	}
	ch := make(chan int)
	var totals []int
	starts := findStarts(nodes)
	for _, start := range starts {
		go walkNode(start, dirs, ch)
	}
	for i := 0; i < len(starts); i += 1 {
		total := <-ch
		totals = append(totals, total)
	}
	total := leastCommonMultiple(totals...)
	return total, nil
}

func greatestCommonDivisor(x, y int) int {
	for y != 0 {
		temp := y
		y = x % y
		x = temp
	}
	return x
}

func leastCommonMultiple(numbers ...int) int {
	if len(numbers) < 2 {
		return numbers[0]
	}
	x := numbers[0]
	y := numbers[1]
	result := x * y / greatestCommonDivisor(x, y)
	for i := 2; i < len(numbers); i += 1 {
		result = leastCommonMultiple(result, numbers[i])
	}
	return result
}

func findStarts(nodes map[string]*Node) (starts []*Node) {
	for _, n := range nodes {
		if []rune(n.name)[2] == 'A' {
			starts = append(starts, n)
		}
	}
	return starts
}

func main() {
	if len(os.Args) > 1 {
		total, err := processInput(os.Args[1])
		if err == nil {
			fmt.Printf("Total: %d\n", total)
		} else {
			fmt.Printf("Problem processing input %s: %v\n", os.Args[1], err)
			os.Exit(-1)
		}
	} else {
		fmt.Printf("Missing required input argument\n")
		os.Exit(-1)
	}
}
