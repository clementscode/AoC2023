package main

import (
	"bufio"
	"fmt"
	"os"
)

const (
	NORTH = iota
	SOUTH
	EAST
	WEST
)

var DIRS = []int{NORTH, SOUTH, EAST, WEST}
var DIR_STR = []string{"NORTH", "SOUTH", "EAST", "WEST"}

var REVERSE = map[int]int{
	NORTH: SOUTH,
	SOUTH: NORTH,
	EAST:  WEST,
	WEST:  EAST,
}

type Coord struct {
	x int
	y int
}

func (c Coord) String() string {
	return fmt.Sprintf("(%d,%d)", c.x, c.y)
}

type Vector struct {
	from Coord
	to   Coord
	dir  int
}

func (v Vector) String() string {
	return fmt.Sprintf("from %v to %v %s", v.from, v.to, DIR_STR[v.dir])
}

type Tile struct {
	label rune
	edges [4]bool
	loop  bool
}

func (t Tile) getExit(entrance int) int {
	for i := 0; i < len(t.edges); i += 1 {
		if t.edges[i] && i != entrance {
			return i
		}
	}
	return -1
}

func walk(c1 Coord, dir int) (c2 Coord) {
	c2 = c1
	switch dir {
	case NORTH:
		c2.y -= 1
	case SOUTH:
		c2.y += 1
	case EAST:
		c2.x += 1
	case WEST:
		c2.x -= 1
	}
	return
}

type Grid struct {
	tiles  []Tile
	width  int
	height int
	start  Coord
}

func (g *Grid) getTile(c Coord) (t Tile, err error) {
	index := (g.width * c.y) + c.x
	if index < 0 || index >= len(g.tiles) {
		err = fmt.Errorf("Invalid coord %d,%d", c.x, c.y)
		return
	}
	return g.tiles[index], nil
}

func (g *Grid) markTile(c Coord) {
	index := (g.width * c.y) + c.x
	g.tiles[index].loop = true
}

func (g *Grid) walkVector(in Vector) (out Vector, err error) {
	t, err := g.getTile(in.to)
	if err != nil {
		err = fmt.Errorf("Walking vector %v %w", in, err)
		return
	}
	g.markTile(in.to)
	out.from = in.to
	out.dir = t.getExit(REVERSE[in.dir])
	out.to = walk(in.to, out.dir)
	g.markTile(out.to)
	return
}

func (g *Grid) addRow(line string) error {
	width := len(line)
	if g.width == 0 {
		g.width = width
	} else if g.width != width {
		return fmt.Errorf("Provided line width %d does not match grid width %d", width, g.width)
	}
	for x, c := range line {
		t := Tile{}
		t.label = c
		switch c {
		case 'S':
			g.start.x = x
			g.start.y = g.height
			t.loop = true
		case '-':
			t.edges[EAST] = true
			t.edges[WEST] = true
		case '7':
			t.edges[WEST] = true
			t.edges[SOUTH] = true
		case '|':
			t.edges[NORTH] = true
			t.edges[SOUTH] = true
		case 'J':
			t.edges[NORTH] = true
			t.edges[WEST] = true
		case 'L':
			t.edges[NORTH] = true
			t.edges[EAST] = true
		case 'F':
			t.edges[EAST] = true
			t.edges[SOUTH] = true
		case '.':
			// nothing
		default:
			return fmt.Errorf("Unexpected character %s", string(c))
		}
		g.tiles = append(g.tiles, t)
	}
	g.height += 1
	return nil
}

func (g *Grid) getStartVectors() (v1, v2 Vector, err error) {
	var index int
	v1, index, err = findNextVector(g, g.start, 0)
	if err != nil {
		return
	}
	v2, index, err = findNextVector(g, g.start, index+1)
	if err != nil {
		return
	}
	var minDir, maxDir int
	if v1.dir > v2.dir {
		minDir = v2.dir
		maxDir = v1.dir
	} else {
		minDir = v1.dir
		maxDir = v2.dir
	}
	var label rune
	if minDir == NORTH && maxDir == SOUTH {
		label = '|'
	} else if minDir == NORTH && maxDir == EAST {
		label = 'L'
	} else if minDir == NORTH && maxDir == WEST {
		label = 'J'
	} else if minDir == SOUTH && maxDir == EAST {
		label = 'F'
	} else if minDir == SOUTH && maxDir == WEST {
		label = '7'
	} else if minDir == EAST && maxDir == WEST {
		label = '-'
	}
	startIndex := (g.width * g.start.y) + g.start.x
	g.tiles[startIndex].label = label
	fmt.Printf("Start label: %s\n", string(label))
	return
}

func findNextVector(g *Grid, tile Coord, start int) (v Vector, index int, err error) {
	v.from = tile
	for i := start; i < len(DIRS); i += 1 {
		c := walk(tile, DIRS[i])
		var t Tile
		t, err = g.getTile(c)
		if err != nil {
			// on edge
			continue
		}
		rev := REVERSE[DIRS[i]]
		if t.edges[rev] {
			v.to = c
			v.dir = DIRS[i]
			index = i
			return
		}
	}
	return
}

func (g *Grid) findEnclosedArea() (int, error) {
	count := 0
	for y := 0; y < g.height; y += 1 {
		// cast ray from west edge and count intersections
		intersections := 0
		corner := '.'
		for x := 0; x < g.width; x += 1 {
			t, err := g.getTile(Coord{x, y})
			if err != nil {
				return -1, err
			}
			if t.loop {
				switch t.label {
				case 'F':
					fallthrough
				case 'L':
					if corner == '.' {
						corner = t.label
					}
				case '7':
					if corner == 'L' {
						intersections += 1
					}
					corner = '.'
				case 'J':
					if corner == 'F' {
						intersections += 1
					}
					corner = '.'
				case '|':
					intersections += 1
				}
			} else if intersections%2 == 1 {
				count += 1
			}
		}
	}
	return count, nil
}

func processInput(f string) (int, error) {
	inputFile, err := os.Open(f)
	if err != nil {
		return -1, fmt.Errorf("Problem reading file at %s %w", f, err)
	}
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)
	var g Grid
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			continue
		}
		err := g.addRow(scanner.Text())
		if err != nil {
			return -1, err
		}
	}
	// get coordinates that start is connected to
	v1, v2, err := g.getStartVectors()
	if err != nil {
		return -1, err
	}
	var count int
	for count = 1; v1.to != v2.to; count += 1 {
		v1, err = g.walkVector(v1)
		if err != nil {
			return -1, err
		}
		v2, err = g.walkVector(v2)
		if err != nil {
			return -1, err
		}
	}
	fmt.Printf("Longest dist %d\n", count)
	return g.findEnclosedArea()
}

func main() {
	if len(os.Args) > 1 {
		total, err := processInput(os.Args[1])
		if err == nil {
			fmt.Printf("Total: %d\n", total)
		} else {
			fmt.Printf("Problem processing input %s: %v\n", os.Args[1], err)
			os.Exit(-1)
		}
	} else {
		fmt.Printf("Missing required input argument\n")
		os.Exit(-1)
	}
}
